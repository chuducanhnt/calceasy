/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import CalcResponse from './component/CalcResponse';
import ButtonContainer from './component/buttonContainer';

type Props = {};
var self;

export default class App extends Component<Props> {
  
  constructor(Props) {
    super(Props);
    this.state = {
      first: '0',
      second: '',
      operator: '',
      result: '0',
      isResult: false,
    };

    self = this;
  }
  getResult = async () => {
    let first = self.state.first;
    let second = self.state.second;
    let operator = self.state.operator;
    const firstNum = parseFloat(first);
    const secondNum = parseFloat(second) || 0;
    let Result = 0;

    console.log(first);
    console.log(second);

    switch (operator) {
      case '+':
        Result = firstNum + secondNum;
        break;
      case '-':
        Result = firstNum - secondNum;
        break;
      case '*':
        Result = firstNum * secondNum;
        break;
      case '/':
        if (secondNum == 0)
          Result = 'error';
        else
          Result = parseFloat(firstNum / secondNum).toFixed(10);
        break;
    }

    console.log("result1 is " + Result);
    await self.setState({
      first: Result,
      second: '',
      operator: '',
      result: Result,
      isResult: true,
    });

    console.log("first is " + self.state.first);
    console.log("second is " + self.state.second);
    console.log("result is " + self.state.result);
  }

  refresh = async () => {
    await self.setState({
      first: '0',
      second: "",
      operator: "",
      result: '0',
      isResult: false,
    })
  }

  handleButtonPress = async (button) => {
    let numFirst = self.state.first;
    let numSecond = self.state.second;
    let Operator = self.state.operator;


    if (Operator) {
      if (button == '+' || button == '-' || button == '*' || button == '/') {
        if (numSecond.length > 0) {
          await self.getResult();
          numFirst = self.state.result;

          console.log(numFirst);

          await self.setState({
            first: numFirst,
            second: '',
            result: numFirst,
            operator: button,
            isResult: true,
          });
        } else {
          await self.setState({
            first: numFirst,
            second: '',
            result: '0',
            operator: button,
            isResult: false,
          })
        }
      } else {
        if (button == '=') {
          if (numSecond.length == 0) {
            alert("input number second");
            return;
          }
          await self.getResult();
          return;
        }
        if (numSecond == "0")
          numSecond = button;
        else
          numSecond = numSecond + button;
        await self.setState({
          second: numSecond,
        })
      }
    } else {
      if (button == '=') {
        alert("input operator and second number");
        return;
      }
      if (button == '+' || button == '-' || button == '*' || button == '/') {
        await self.setState({
          first: numFirst,
          second: '',
          result: '0',
          operator: button,
          isResult: false,
        })
        return;
      }
      if (numFirst == "0")
        numFirst = button;
      else
        numFirst = numFirst + button;
      await self.setState({
        first: numFirst,
        second: '',
        result: '0',
        operator: '',
        isResult: false,
      })
    }
  }

  render() {
    const { first, second, operator, result } = this.state;

    return (
      <View style={styles.Container}>
        <CalcResponse
          first={first}
          second={second}
          operator={operator}
          result={result}
          refresh={this.refresh}
        />

        <ButtonContainer
          handleButtonPress={(operator) => { this.handleButtonPress(operator) }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: 'blue',
  },
});